var lastopenedwiki = 0;

function findGetParameter(parameterName) {
    var result = null,
        tmp = [];
    location.search
        .substr(1)
        .split("&")
        .forEach(function(item) {
            tmp = item.split("=");
            if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
        });
    if (result != null && result != '') {
        return result;
    } else {
        return 'abcdefghvklmnc';
    }
}

function html(html) {
    return html.replace(/<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi, '')
        .replace(/<style\b[^<]*(?:(?!<\/style>)<[^<]*)*<\/style>/gi, '')
        .replace(/"items"/gi, '')
        .replace(/"snippet"/gi, '')
        .replace(/href="(.*?)"/gi, ' ')
        .replace(/   (.*)nbsp/gi, ' ')
        .replace(/(<([^>]+)>)/ig, ' ')
        .replace(/[^a-zA-Z0-9а-яА-Я% ]/g, ' ');
}

Array.prototype.remove = function(value) {
    var idx = this.indexOf(value);

    if (idx != -1) {
        return this.splice(idx, 1);
    }

    return false;
}

if (window.location.hostname.includes('connorbot')) {
    $("#ext")[0].innerHTML = chrome.runtime.id;
}

if (document.location.href.includes("connorbot=true") &&
    window.location.hostname.includes('yandex') && !document.location.href.includes("connormulti=true")) {

    var connorbotmessage = document.createElement('div');

    chrome.runtime.onMessage.addListener(
        function(request, sender, sendResponse) {

            var cbm = '';

            if (request["ai_answer_id"] != null && request["ai_answer_text"] != null) {

                document.title = request["ai_answer_id"] + ' - ' + request["ai_answer_text"];

                var cbm = cbm + request["ai_answer_id"] + '. ' + request["ai_answer_text"];
                var cbm = cbm + '<br><br>';

                if (request["variables"]["1"] != null) {
                    var cbm = cbm + '1. ' + request["variables"]["1"]["text"] + ' - score ' + request["variables"]["1"]["score"];
                    var cbm = cbm + '<br>';
                }
                if (request["variables"]["2"] != null) {
                    var cbm = cbm + '2. ' + request["variables"]["2"]["text"] + ' - score ' + request["variables"]["2"]["score"];
                    var cbm = cbm + '<br>';
                }
                if (request["variables"]["3"] != null) {
                    var cbm = cbm + '3. ' + request["variables"]["3"]["text"] + ' - score ' + request["variables"]["3"]["score"];
                    var cbm = cbm + '<br>';
                }
                if (request["variables"]["4"] != null) {
                    var cbm = cbm + '4. ' + request["variables"]["4"]["text"] + ' - score ' + request["variables"]["4"]["score"];
                    var cbm = cbm + '<br>';
                }

                connorbotmessage.innerHTML = cbm;
            } else {
                if (request["ai_answer_text"] == null && request["ai_answer_id"] == null && request["variables"] != null) {
                    if (request["question_text"] != null) {
                        var cbm = cbm + request["question_text"];
                        var cbm = cbm + '<br><br>';
                    }
                    if (request["variables"]["1"] != null) {
                        var cbm = cbm + '1. ' + request["variables"]["1"]["text"];
                        var cbm = cbm + '<br>';
                    }
                    if (request["variables"]["2"] != null) {
                        var cbm = cbm + '2. ' + request["variables"]["2"]["text"];
                        var cbm = cbm + '<br>';
                    }
                    if (request["variables"]["3"] != null) {
                        var cbm = cbm + '3. ' + request["variables"]["3"]["text"];
                        var cbm = cbm + '<br>';
                    }
                    if (request["variables"]["4"] != null) {
                        var cbm = cbm + '4. ' + request["variables"]["4"]["text"];
                        var cbm = cbm + '<br>';
                    }

                    connorbotmessage.innerHTML = cbm;
                }
            }

            if (request["wiki_link"] != null) {
                if(lastopenedwiki != request["question_id"]){
                    lastopenedwiki = request["question_id"];
                    chrome.runtime.sendMessage({
                       'type': 'OPENTABNOSWITCH',
                        'url': request["wiki_link"] + '?&connorbot=true'
                    });
                }
            }
        });

    document.title = 'YaConnary - Улучшенный автопоиск';

    $('.link').each(function(i, obj) {
    	if($(obj).parent()[0].className != "object-figures__thumb-inner"){
    		$(obj).addClass('connorbothidden');
    	}
    });

    $('.organic__subtitle').each(function(i, obj) {
        $(obj).addClass('connorbothidden');
    });

    $('.extended-text__toggle').each(function(i, obj) {
        $(obj).addClass('connorbothidden');
    });

    $('.extended-text__full').each(function(i, obj) {
        obj.style = "display: block; color: #d3d3d3;";
    });

    $('.extended-text__short').each(function(i, obj) {
        $(obj).addClass('connorbothidden');
    });

    $('.entity-search__list').each(function(i, obj) {
        $(obj).addClass('connorbothidden');
    });

    $('.object-footer_separated_yes').each(function(i, obj) {
        $(obj).addClass('connorbothidden');
    });

    $('.favicon__icon').each(function(i, obj) {
        $(obj).addClass('connorbothidden');
    });

    $('.more-button').each(function(i, obj) {
        $(obj).addClass('connorbothidden');
    });

    $('.fact-layout__object-footer').each(function(i, obj) {
        $(obj).addClass('connorbothidden');
    });

    var styleelem = document.createElement('style');
    styleelem.innerHTML = '.connorbotmessage{position: fixed;bottom:0;right:0;font-size: 16px;margin: 100px;margin-right: 200px; background-image: linear-gradient(60deg, #3030308a, #3030308a); border-radius: 5px; padding: 10px; color: white;}';
    styleelem.innerHTML = styleelem.innerHTML + '.connorbotanswerbbl{background-image: linear-gradient(60deg, #ffffff, #ffffff); color: #000000; border-radius: 4px; margin-right: 2px;}';
    styleelem.innerHTML = styleelem.innerHTML + '.connorbotshape{background-image: linear-gradient(60deg, #303030b8, #303030b8); border-radius: 5px; padding: 10px; color: white;}';
    styleelem.innerHTML = styleelem.innerHTML + '.connorbotfulltext{display: block; color: #d3d3d3;}';
    styleelem.innerHTML = styleelem.innerHTML + '.connorbothidden{display: none;}';
    styleelem.innerHTML = styleelem.innerHTML + '.connorbotb{color: #dac15b;}';
    $("body")[0].append(styleelem);

    $('.organic__url-text').each(function(i, obj) {
        if (!obj.innerHTML.includes("Википедия")) {
            obj.style = 'color: #ffcc00;';
        } else {
            obj.style = 'color: #ffffff;';
        }
    });

    if ($(".serp-list_left_yes") != null) {
        $(".serp-list_left_yes").attr('id', 'oldlistleft');
        $(".serp-list_left_yes").clone().prop('id', 'newlistleft').insertBefore(".serp-list_left_yes");
    }

    if (findGetParameter('answer1') != null) {
        var spltlc1 = findGetParameter('answer1').replace(/[^a-zA-Z0-9а-яА-Яё% ]/g, '').toLowerCase().split(" ");
    }
    if (findGetParameter('answer2') != null) {
        var spltlc2 = findGetParameter('answer2').replace(/[^a-zA-Z0-9а-яА-Яё% ]/g, '').toLowerCase().split(" ");
    }
    if (findGetParameter('answer3') != null) {
        var spltlc3 = findGetParameter('answer3').replace(/[^a-zA-Z0-9а-яА-Яё% ]/g, '').toLowerCase().split(" ");
    }
    if (findGetParameter('answer4') != null) {
        var spltlc4 = findGetParameter('answer4').replace(/[^a-zA-Z0-9а-яА-Яё% ]/g, '').toLowerCase().split(" ");
    }

    $('.extended-text__full').each(function(i, obj) {
        let promise = new Promise(function(resolve, reject) {
            resolve(html(obj.innerHTML).split(' '));
        }).then(function(r) {
            for (i = 0; i < r.length; i++) {
                var fltr = r[i].replace(/[^a-zA-Z0-9а-яА-Яё% ]/g, '').toLowerCase();
                var fltrlength = fltr.length - 1;
                if (findGetParameter('answer1') != null) {
                    spltlc1.forEach(function(elem) {
                        if (fltr.includes(elem) && elem.length >= fltrlength) {
                            obj.innerHTML = obj.innerHTML.replace('«' + r[i], '«<i class="connorbotanswerbbl">' + ' ⒈' + r[i] + '</i>');
                            obj.innerHTML = obj.innerHTML.replace('>' + r[i], '><i class="connorbotanswerbbl">' + ' ⒈' + r[i] + '</i>');
                            obj.innerHTML = obj.innerHTML.replace(' ' + r[i], '<i class="connorbotanswerbbl">' + ' ⒈' + r[i] + '</i>');
                        }
                    });
                }
                if (findGetParameter('answer2') != null) {
                    spltlc2.forEach(function(elem) {
                        if (fltr.includes(elem) && elem.length >= fltrlength) {
                            obj.innerHTML = obj.innerHTML.replace('«' + r[i], '«<i class="connorbotanswerbbl">' + ' ⒉' + r[i] + '</i>');
                            obj.innerHTML = obj.innerHTML.replace('>' + r[i], '><i class="connorbotanswerbbl">' + ' ⒉' + r[i] + '</i>');
                            obj.innerHTML = obj.innerHTML.replace(' ' + r[i], '<i class="connorbotanswerbbl">' + ' ⒉' + r[i] + '</i>');
                        }
                    });
                }
                if (findGetParameter('answer3') != null) {
                    spltlc3.forEach(function(elem) {
                        if (fltr.includes(elem) && elem.length >= fltrlength) {
                            obj.innerHTML = obj.innerHTML.replace('«' + r[i], '«<i class="connorbotanswerbbl">' + ' ⒊' + r[i] + '</i>');
                            obj.innerHTML = obj.innerHTML.replace('>' + r[i], '><i class="connorbotanswerbbl">' + ' ⒊' + r[i] + '</i>');
                            obj.innerHTML = obj.innerHTML.replace(' ' + r[i], '<i class="connorbotanswerbbl">' + ' ⒊' + r[i] + '</i>');
                        }
                    });
                }
                if (findGetParameter('answer4') != null) {
                    spltlc4.forEach(function(elem) {
                        if (fltr.includes(elem) && elem.length >= fltrlength) {
                            obj.innerHTML = obj.innerHTML.replace('«' + r[i], '«<i id="thereis" class="connorbotanswerbbl">' + ' ⒋' + r[i] + '</i>');
                            obj.innerHTML = obj.innerHTML.replace('>' + r[i], '><i id="thereis" class="connorbotanswerbbl">' + ' ⒋' + r[i] + '</i>');
                            obj.innerHTML = obj.innerHTML.replace(' ' + r[i], '<i id="thereis" class="connorbotanswerbbl">' + ' ⒋' + r[i] + '</i>');
                        }
                    });
                }
            }
        });
    });

    $('.serp-item').each(function(i, obj) {
        if (obj.innerHTML.includes("wikipedia.org")) {
            $(obj).addClass('connorbotshape');
        }
    });

    $('.text-container').each(function(i, obj) {
        $(obj).addClass('connorbotfulltext');
    });

    $('.fact').each(function(i, obj) {
        $(obj).addClass('connorbotshape');
    });

    $('.composite_gap_s').each(function(i, obj) {
        if (obj.innerHTML.includes("music.yandex.ru")) {
            $(obj).addClass('connorbothidden');
        }
    });

    $('.serp-item').each(function(i, obj) {
        if (obj.innerHTML.includes("youtube.com") || obj.innerHTML.includes("clip") || obj.innerHTML.includes("usvid") || obj.innerHTML.includes("/video/")) {
            $(obj).addClass('connorbothidden');
        }
    });

    $('b').each(function(i, obj) {
        $(obj).addClass('connorbotb');
    });

    var imsg = $('.serp-item');
    imsg.each(function(i, obj) {
        if (!obj.innerHTML.includes('serp-title_type_supertitle')) {
            if (obj.innerHTML.toLowerCase().includes(findGetParameter('answer1').toLowerCase()) || obj.innerHTML.toLowerCase().includes(findGetParameter('answer2').toLowerCase()) || obj.innerHTML.toLowerCase().includes(findGetParameter('answer3').toLowerCase()) || obj.innerHTML.toLowerCase().includes(findGetParameter('answer4').toLowerCase())) {
                if (imsg[i].parentNode.id == "newlistleft") {
                    $(obj).addClass('connorbotshape');
                } else {
                    $(obj).addClass('connorbothidden');
                }
            } else {
                if (imsg[i].parentNode.id == "newlistleft") {
                    $(obj).addClass('connorbothidden');
                }
            }
        }
    });

    $(".paranja_state_close").addClass('connorbothidden');
    $(".serp-navigation").addClass('connorbothidden');
    $(".serp-header").addClass('connorbothidden');
    if ($(".footer")[0] != null) {
        $(".footer").remove();
    }
    if ($(".serp-footer__main")[0] != null) {
        $(".serp-footer__main").remove();
    }
    $(".region-change").addClass('connorbothidden');
    $(".competitors").addClass('connorbothidden');
    if ($(".related")[0] != null) {
        $(".related").addClass('connorbothidden');
    }

    $(connorbotmessage).toggleClass('connorbotmessage');
    $(connorbotmessage).attr('id', 'cbmsg');
    connorbotmessage.innerHTML = 'Удачной игры!';
    $("body").append(connorbotmessage);
    $("body")[0].style = "background-image: url(https://desktop.github.com/images/star-bg.svg),linear-gradient(#475aa0, #5e8a88 75%); background-repeat: no-repeat; background-attachment: fixed;";
    if ($(".serp-title")[0] != null) {
        $(".serp-title")[0].style = "margin: 5px; margin-bottom: -15px; font-size: 21px;";
    }
    if ($(".fact__title")[0] != null) {
        $(".fact__title").addClass('connorbothidden');
        $(".fact__path").addClass('connorbothidden');
        $(".fact__answer")[0].style = "font-size: 16px;";
    }
    if ($(".homonyms")[0] != null) {
        $(".homonyms").addClass('connorbothidden');
    }
    if ($(".entity-search__homonyms")[0] != null) {
        $(".entity-search__homonyms").remove();
    }
    if ($(".distr-default-search__content")[0] != null) {
        $(".distr-default-search__content").remove();
    }
    if ($(".serp-adv__displayed")[0] != null) {
        $(".serp-adv__displayed").addClass('connorbothidden');
    }
    if ($(".serp-adv__found")[0] != null) {
        $(".serp-adv__found").addClass('connorbothidden');
    }
    if ($(".misspell")[0] != null) {
        $(".misspell").addClass('connorbothidden');
    }
    $(".link:contains('Дать объявление')").addClass('connorbothidden');

    if (findGetParameter('text').match(/( из | of these | термин )/ig)) {
        if (findGetParameter('answer1') != 'abcdefghvklmnc' && findGetParameter('answer1') != 'Оба варианта') {
            chrome.runtime.sendMessage({
                'type': 'OPENTABNOSWITCH',
                'url': 'https://yandex.ru/search/?text=' + findGetParameter('answer1') + '&connorbot=true&connormulti=true'
            });
        }
        if (findGetParameter('answer2') != 'abcdefghvklmnc' && findGetParameter('answer2') != 'Оба варианта') {
            chrome.runtime.sendMessage({
                'type': 'OPENTABNOSWITCH',
                'url': 'https://yandex.ru/search/?text=' + findGetParameter('answer2') + '&connorbot=true&connormulti=true'
            });
        }
        if (findGetParameter('answer3') != 'abcdefghvklmnc' && findGetParameter('answer3') != 'Оба варианта') {
            chrome.runtime.sendMessage({
                'type': 'OPENTABNOSWITCH',
                'url': 'https://yandex.ru/search/?text=' + findGetParameter('answer3') + '&connorbot=true&connormulti=true'
            });
        }
        if (findGetParameter('answer4') != 'abcdefghvklmnc' && findGetParameter('answer4') != 'Оба варианта') {
            chrome.runtime.sendMessage({
                'type': 'OPENTABNOSWITCH',
                'url': 'https://yandex.ru/search/?text=' + findGetParameter('answer4') + '&connorbot=true&connormulti=true'
            });
        }
    }

}